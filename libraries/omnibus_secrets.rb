# Module to fetch all omnibus secrets

module OmnibusSecrets
  def redis_password
    secrets_config = node.read('omnibus-gitlab', 'secrets', 'all-the-omnibus-secrets')
    return unless secrets_config

    omnibus_secrets = get_secrets(
      secrets_config['backend'],
      secrets_config['path'],
      secrets_config['key']
    )

    redis_secrets = omnibus_secrets['omnibus-gitlab']['gitlab_rb']['redis']
    redis_secrets['password']
  end

  def postgres_exporter_password
    return unless node.read('postgres_exporter', 'secrets', 'backend')

    get_secrets(
      node['postgres_exporter']['secrets']['backend'],
      node['postgres_exporter']['secrets']['path'],
      node['postgres_exporter']['secrets']['key']
    )['exporter_user_password']
  end

  # Call this method inside a template resource to
  # add helper methods to templates for providing
  # secrets.
  # Helpers are preferable to variables, as they're
  # evaluated late rather than early, meaning that we
  # won't try resolve secrets when we don't need to
  #  use them in a template.
  def bind_secrets_helpers
    context = self
    helper(:redis_password) { context.redis_password }
    helper(:postgres_exporter_password) { context.postgres_exporter_password }
  end
end

Chef::Resource::Template.include OmnibusSecrets
