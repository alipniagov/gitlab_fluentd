# frozen_string_literal: true

Dir.glob(File.join(File.dirname(__FILE__), '**/*.rb')).map { |file| require file }
