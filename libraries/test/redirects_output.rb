# frozen_string_literal: true

module RedirectsOutput
  def redirect_output(config_path, target_path)
    original_config = load_config(config_path)
    output_config = redirect_config(target_path)

    return if original_config.include?(output_config)

    new_config = original_config.sub(%r{</match>}, "#{output_config}\n</match>")

    write_config(config_path, new_config)
  end

  def redirect_config(path)
    <<~OUT
      <store>
        @type file
        path #{path}-out
        symlink_path #{path}
        append true
        <buffer>
          @type file
          timekey 1h
          timekey_wait 1s
        </buffer>
        <format>
          @type json
        </format>
      </store>
    OUT
  end

  def read_new_log_line(path, &generate_log_line)
    before = inspec.file(path).content&.lines&.last

    wait_until do
      generate_log_line.call
      new_line = inspec.file(path).content&.lines&.last
      break new_line if new_line && new_line != before
    end
  end
end
