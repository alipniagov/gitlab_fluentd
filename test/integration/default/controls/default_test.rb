# frozen_string_literal: true

# InSpec tests for recipe gitlab_fluentd::default

control 'general-checks' do
  impact 1.0
  title 'General tests for gitlab_fluentd cookbook'
  desc '
    This control ensures that:
      * td-agent is running'

  describe service('td-agent') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end
end

control 'td-agent-environments-valid' do
  impact 1.0
  title '/etc/td-agent/td-agent.environment exists and has the right permissions'

  describe file('/etc/td-agent/td-agent.environment') do
    it { should exist }
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
    its('mode') { should cmp '0600' }
  end
end

control 'logrotate-config-valid' do
  impact 1.0
  title 'apt supplied config is overwritten'

  describe file('/etc/logrotate.d/td-agent') do
    it { should exist }
    its('content') { should include 'daily' }
    its('content') { should include 'rotate 7' }
    its('content') { should include 'size 1G' }
    its('content') { should include 'postrotate' }
    its('content') { should include 'endscript' }
  end
end

control 'bundle-check' do
  impact 1.0
  title 'bundle check all td-agent gem dependencies are installed'

  describe td_agent_bundle do
    it { should be_installed }
    it { should have_config('BUNDLE_PATH' => '/var/lib/td-agent/vendor/bundle') }
  end
end

control '/etc/td-agent/Gemfile.lock is owned by root and has the right permissions' do
  impact 1.0
  title '/etc/td-agent/Gemfile.lock is owned by root and has the right permissions'

  describe file('/etc/td-agent/Gemfile.lock') do
    it { should exist }
    it { should be_owned_by 'root' }
    it { should be_grouped_into 'root' }
    its('mode') { should cmp '0644' }
  end
end
