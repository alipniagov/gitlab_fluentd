# apt repo settings
default['gitlab_fluentd']['apt_repo_platform'] = node['platform']
default['gitlab_fluentd']['apt_repo_distribution'] = node['lsb']['codename']
default['gitlab_fluentd']['apt_repo_uri'] = "http://packages.treasuredata.com/4/#{node['gitlab_fluentd']['apt_repo_platform']}/#{node['gitlab_fluentd']['apt_repo_distribution']}"
default['gitlab_fluentd']['apt_repo_components'] = ['contrib']
default['gitlab_fluentd']['apt_repo_key'] = 'https://packages.treasuredata.com/GPG-KEY-td-agent'

default['gitlab_fluentd']['td-agent']['version'] = '4.0.1-1'

# output settings for "google_cloud" stackdriver plugin:
# When Stackdriver is not accepting logs as quickly as we are generating them, fluentd can
# locally buffer log records in memory up to a configurable max queue size.  This queue is
# implemented as a set of buffer chunks, each of which is "buffer_chunk_limit" bytes, and
# the max number of those chunks is "buffer_queue_limit".  So:
#     max queue size is bytes = [buffer_chunk_limit] bytes/chunk * [buffer_queue_limit] chunks
# Note: Setting "total_limit_size" is ineffective because "buffer_queue_limit" overrides it.
# For more details, see: https://gitlab.com/gitlab-com/gl-infra/production/-/issues/5754#note_710883345
default['gitlab_fluentd']['buffer_chunk_limit'] = '8m'
default['gitlab_fluentd']['buffer_queue_limit'] = '64'
default['gitlab_fluentd']['flush_interval'] = '60'
default['gitlab_fluentd']['log_level'] = 'info'

# elasticsearch output settings
default['gitlab_fluentd']['stackdriver_enable'] = false

# pubsub output settings
default['gitlab_fluentd']['enable_td_agent_log_consumption'] = false
default['gitlab_fluentd']['google_cloud_monitoring'] = false
default['gitlab_fluentd']['pubsub_enable'] = false
default['gitlab_fluentd']['pubsub_enable_nginx'] = false
default['gitlab_fluentd']['pubsub_enable_haproxy'] = false
default['gitlab_fluentd']['pubsub_env'] = ''
default['gitlab_fluentd']['pubsub_project'] = ''
default['gitlab_fluentd']['pubsub_key'] = nil
default['gitlab_fluentd']['pubsub_file'] = '/etc/td-agent/pubsub-gcp-key.json'
# Override the environment that is set in fluentd,
# defaults to the node's env.
default['gitlab_fluentd']['pubsub_log_env'] = nil

# directory settings
default['gitlab_fluentd']['config_dir_modules'] = '/etc/td-agent/conf.d'
default['gitlab_fluentd']['es_template_dir'] = '/etc/td-agent/es-templates'
default['gitlab_fluentd']['log_dir'] = '/var/log/td-agent'
default['gitlab_fluentd']['gemfile'] = '/etc/td-agent/Gemfile'
default['gitlab_fluentd']['gem_path'] = '/var/lib/td-agent/vendor/bundle'
default['gitlab_fluentd']['plugin_path'] = '/etc/td-agent/plugin'

# Postgres settings
default['gitlab_fluentd']['enable_postgres_csvlog_ingestion'] = false
default['gitlab_fluentd']['postgres_log_path'] = '/var/log/gitlab/postgresql/current'
default['gitlab_fluentd']['postgres_csvlog_path'] = '/var/log/gitlab/postgresql/*.csv'
default['gitlab_fluentd']['pgbouncer_log_path'] = '/var/log/gitlab/pgbouncer/current'
default['gitlab_fluentd']['walg_log_archive_path'] = '/var/log/wal-g/wal-g.log'
default['gitlab_fluentd']['walg_log_basebackup_path'] = '/var/log/wal-g/wal-g_backup_push.log'
default['gitlab_fluentd']['repmgrd_enabled'] = true
default['gitlab_fluentd']['wale_enabled'] = false

# Postgres connection details shared by pg_stat_statements and pg_stat_activity
default['gitlab_fluentd']['postgres_input_host'] = 'localhost'
default['gitlab_fluentd']['postgres_input_port'] = 5432
default['gitlab_fluentd']['postgres_input_username'] = 'postgres_exporter'
default['gitlab_fluentd']['postgres_input_dbname'] = 'gitlabhq_production'
default['gitlab_fluentd']['postgres_input_sslmode'] = 'prefer'

# Postgres pg_stat_statements input plugin
default['gitlab_fluentd']['postgres_pg_stat_statements_enable'] = false
default['gitlab_fluentd']['postgres_pg_stat_statements_interval'] = 1800

# Postgres pg_stat_activity input plugin
default['gitlab_fluentd']['postgres_pg_stat_activity_enable'] = false
default['gitlab_fluentd']['postgres_pg_stat_activity_interval'] = 120

default['gitlab_fluentd']['redis_slowlog_enabled'] = false

default['gitlab_fluentd']['log_parsing_format'] = 'plain'

default['gitlab_fluentd']['workers'] = nil

# OSQuery settings
default['gitlab_fluentd']['osquery_results_paths'] = '/var/log/osquery/osqueryd.results.log,/var/log/osquery/osqueryd.snapshots.log'
