require 'chefspec'
require 'chefspec/berkshelf'
require './libraries/omnibus_secrets'

RSpec.configure do |config|
  config.before(:each) do
    # Stub common secrets
    allow_any_instance_of(Chef::Recipe).to receive(:get_secrets)
      .with('fake_backend', 'fake_path', 'fake_key')
      .and_return('elastic_cloud_host' => '1.2.3.4',
                  'elastic_cloud_port' => '1234',
                  'elastic_cloud_user' => 'root',
                  'elastic_cloud_password' => 'toor',
                  'pubsub_key' => '{"foo": "bar"}')
  end
end
