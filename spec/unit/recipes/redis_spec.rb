require 'spec_helper'

describe 'gitlab_fluentd::redis' do
  platform 'ubuntu', '18.04'

  normal_attributes['gitlab_fluentd']['stackdriver_enable'] = true
  normal_attributes['gitlab_fluentd']['pubsub_enable'] = true
  normal_attributes['gitlab_fluentd']['pubsub_env'] = 'prd'
  normal_attributes['gitlab_fluentd']['pubsub_project'] = 'production'
  normal_attributes['gitlab-server'] = {
    'rsyslog_client' => {
      'secrets' => {
        'backend' => 'fake_backend',
        'path' => 'fake_path',
        'key' => 'fake_key',
      },
    },
  }

  before do
    allow_any_instance_of(OmnibusSecrets)
      .to receive(:redis_password)
      .and_return('p@55w0rd!')
  end

  context 'with slowlog enabled' do
    normal_attributes['gitlab_fluentd']['redis_slowlog_enabled'] = true

    it 'renders the slowlog input in the config' do
      slowlog_input = <<~'INPUT'
        <source>
          @type redis_slowlog
          tag redis.slowlog
          password "#{ENV['FLUENTD_REDIS_PASSWORD']}"
        </source>
      INPUT

      td_agent_environment = <<~INPUT
        FLUENTD_REDIS_PASSWORD=p@55w0rd!
      INPUT

      expect(chef_run).to(render_file('/etc/td-agent/conf.d/redis.conf').with_content do |content|
        expect(content).to include(slowlog_input)
      end)

      expect(chef_run).to(render_file('/etc/td-agent/td-agent.environment').with_content do |content|
        expect(content).to include(td_agent_environment)
      end)
    end
  end
end
