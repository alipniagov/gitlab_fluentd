# Cookbook:: gitlab_fluentd
# Spec:: default
#
# Copyright:: 2017, GitLab B.V., MIT.

require 'spec_helper'

describe 'gitlab_fluentd::default' do
  context 'when all attributes are default, on Ubuntu 18.04' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu',
                                 version: '18.04') do |node|
        node.normal['gitlab-server'] = {
          'rsyslog_client' => {
            'secrets' => {
              'backend' => 'fake_backend',
              'path' => 'fake_path',
              'key' => 'fake_key',
            },
          },
        }
      end.converge(described_recipe)
    end

    before do
      # Avoid situations like https://gitlab.com/gitlab-com/gl-infra/production/-/issues/5056
      allow_any_instance_of(OmnibusSecrets)
        .to receive(:postgres_exporter_password)
        .and_raise('postgres_exporter_password should not be called')
    end

    it 'converges succesfully' do
      expect { chef_run }.to_not raise_error
    end

    it 'creates the sessions file' do
      expect(chef_run).to render_file('/etc/td-agent/conf.d/sessions.conf').with_content { |content|
        expect(content).to include '@type null'
      }
    end

    it 'creates and manages the module config dir' do
      expect(chef_run).to create_directory('/etc/td-agent/conf.d')
      expect(chef_run).to clean_managed_directory('/etc/td-agent/conf.d')
    end

    it 'does not by default create the pubsub plugin' do
      expect(chef_run).to_not create_file('/etc/td-agent/plugin/out_cloud_pubsub.rb')
    end

    it 'creates the Gemfile and Gemfile.lock' do
      expect(chef_run).to render_file('/etc/td-agent/Gemfile')
      expect(chef_run).to render_file('/etc/td-agent/Gemfile.lock')
    end

    it 'does not install libpq-dev' do
      expect(chef_run).not_to install_apt_package('libpq-dev')
    end

    context 'when td-agent v3 is installed' do
      before(:each) do
        allow(File).to receive(:exist?).with(anything).and_call_original
        allow(File).to receive(:exist?).with('/etc/systemd/system/td-agent.service').and_return(true)
      end

      it 'stops the old td-agent' do
        expect(chef_run).to run_execute('stop old td-agent')
      end

      it 'deletes the obsolete systemd service' do
        expect(chef_run).to delete_file('/etc/systemd/system/td-agent.service')
      end
    end
  end

  context 'when pubsub is enabled on Ubuntu 18.04' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu',
                                 version: '18.04') do |node|
        node.normal['gitlab_fluentd']['pubsub_enable'] = true
        node.normal['gitlab-server'] = {
          'rsyslog_client' => {
            'secrets' => {
              'backend' => 'fake_backend',
              'path' => 'fake_path',
              'key' => 'fake_key',
            },
          },
        }
      end.converge(described_recipe)
    end

    it 'converges succesfully' do
      expect { chef_run }.to_not raise_error
    end

    it 'creates the pubsub plugin' do
      expect(chef_run).to create_cookbook_file('/etc/td-agent/plugin/out_cloud_pubsub.rb')
      expect(chef_run).to(render_file('/etc/td-agent/plugin/out_cloud_pubsub.rb').with_content do |c|
        expect(c).to include('Originally copied from https://github.com/yosssi/fluent-plugin-cloud-pubsub')
      end)
    end
  end

  context 'when pubsub is enabled with a secret key on Ubuntu 18.04' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu',
                                 version: '18.04') do |node|
        node.normal['gitlab_fluentd']['pubsub_enable'] = true
        node.normal['gitlab-server'] = {
          'rsyslog_client' => {
            'secrets' => {
              'backend' => 'fake_backend',
              'path' => 'fake_path',
              'key' => 'fake_key',
            },
          },
        }
      end.converge(described_recipe)
    end

    it 'converges succesfully' do
      expect { chef_run }.to_not raise_error
    end

    it 'creates the pubsub key file' do
      expect(chef_run).to create_file('/etc/td-agent/pubsub-gcp-key.json').with(
        content: '{"foo": "bar"}'
      )
    end
  end

  context 'when stackdriver is enabled' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu',
                                 version: '18.04') do |node|
        node.normal['gitlab_fluentd']['stackdriver_enable'] = true
        node.normal['gitlab-server'] = {
          'rsyslog_client' => {
            'secrets' => {
              'backend' => 'fake_backend',
              'path' => 'fake_path',
              'key' => 'fake_key',
            },
          },
        }
      end.converge(described_recipe)
    end

    it 'converges succesfully' do
      expect { chef_run }.to_not raise_error
    end

    it 'creates the sessions file' do
      expect(chef_run).to render_file('/etc/td-agent/conf.d/sessions.conf').with_content { |content|
        expect(content).to include '@type google_cloud'
      }
    end

    it 'adds td-agent Gemfile' do
      expect(chef_run).to render_file('/etc/td-agent/Gemfile')
    end
  end
end
