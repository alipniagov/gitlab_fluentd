# Cookbook:: gitlab_fluentd
# Recipe:: redis
# License:: MIT
#
# Copyright:: 2018, GitLab Inc.
include_recipe 'gitlab_fluentd::default'

template File.join(node['gitlab_fluentd']['config_dir_modules'], 'redis.conf') do
  owner 'root'
  group 'root'
  mode '0644'
  bind_secrets_helpers # Injects helpers for secrets from OmnibusSecrets
  notifies :restart, 'service[td-agent]', :delayed
end
