# Cookbook:: gitlab_fluentd
# Recipe:: default
# License:: MIT
#
# Copyright:: 2018, GitLab Inc.

# Fetch secrets from gitlab_server

if node['gitlab-server']['rsyslog_client'].attribute?('secrets')
  elastic_cloud_conf = get_secrets(node['gitlab-server']['rsyslog_client']['secrets']['backend'],
                                   node['gitlab-server']['rsyslog_client']['secrets']['path'],
                                   node['gitlab-server']['rsyslog_client']['secrets']['key'])
  node.default['gitlab_fluentd']['pubsub_key'] = elastic_cloud_conf['pubsub_key']
end

apt_repository 'fluentd' do
  uri           node['gitlab_fluentd']['apt_repo_uri']
  distribution  node['gitlab_fluentd']['apt_repo_distribution']
  components    node['gitlab_fluentd']['apt_repo_components']
  key           node['gitlab_fluentd']['apt_repo_key']
end

apt_package 'build-essential'

# Stop old td-agent and clean up obsolete systemd unit.
execute 'stop old td-agent' do
  command 'systemctl stop td-agent.service'
  only_if { ::File.exist?('/etc/systemd/system/td-agent.service') }
end

file '/etc/systemd/system/td-agent.service' do
  action :delete
  notifies :run, 'execute[systemd reload]', :immediately
  notifies :restart, 'service[td-agent]', :delayed
end

directory '/etc/systemd/system/td-agent.service.d' do
  mode '0755'
  recursive true
end

template '/etc/systemd/system/td-agent.service.d/override.conf' do
  source 'systemd-override.conf.erb'
  mode '0644'
  notifies :run, 'execute[systemd reload]', :immediately
  notifies :restart, 'service[td-agent]', :delayed
end

directory '/etc/td-agent' do
  mode '0755'
  owner 'root'
  group 'root'
end

template '/etc/td-agent/td-agent.environment' do
  source 'td-agent.environment.erb'
  owner 'root'
  group 'root'
  mode '0600'
  bind_secrets_helpers # Injects helpers for secrets from OmnibusSecrets
  notifies :restart, 'service[td-agent]', :delayed
end

execute 'systemd reload' do
  command 'systemctl daemon-reload'
  action :nothing
end

apt_package 'td-agent' do
  version node['gitlab_fluentd']['td-agent']['version']
end

directory node['gitlab_fluentd']['config_dir_modules'] do
  mode '0755'
  owner 'root'
  group 'root'
  subscribes :create, 'apt_package[td-agent]', :immediately
end

managed_directory node['gitlab_fluentd']['config_dir_modules'] do
  action :clean
  notifies :restart, 'service[td-agent]', :delayed
end

directory node['gitlab_fluentd']['es_template_dir'] do
  mode '0755'
  owner 'root'
  group 'root'
  subscribes :create, 'apt_package[td-agent]', :immediately
end

directory node['gitlab_fluentd']['log_dir'] do
  mode '0755'
  owner 'td-agent'
  group 'td-agent'
  subscribes :create, 'apt_package[td-agent]', :immediately
end

# td-agent v3 and v4 set the owner of /etc/td-agent to user td-agent.
# For some reason, some nodes have changed the owner to root. We need
# this to be owned by td-agent so the bundle install can write to
# /etc/td-agent/.bundle.
directory '/etc/td-agent' do
  mode '0755'
  owner 'td-agent'
  group 'td-agent'
end

directory '/var/lib/td-agent/vendor/bundle' do
  mode '0755'
  recursive true
  owner 'td-agent'
  group 'td-agent'
end

template '/etc/td-agent/td-agent.conf' do
  owner 'root'
  group 'root'
  mode '0644'
  notifies :restart, 'service[td-agent]', :delayed
end

# This is needed to allow TD_AGENT_OPTIONS to be defined in override.conf
file '/etc/default/td-agent' do
  action :delete
  notifies :restart, 'service[td-agent]', :delayed
end

template File.join(node['gitlab_fluentd']['config_dir_modules'], 'sessions.conf') do
  owner 'root'
  group 'root'
  mode '0644'
  notifies :restart, 'service[td-agent]', :delayed
end

file node['gitlab_fluentd']['pubsub_file'] do
  owner 'root'
  group 'root'
  mode '0600'
  content node['gitlab_fluentd']['pubsub_key']
  notifies :restart, 'service[td-agent]', :delayed
  not_if { node['gitlab_fluentd']['pubsub_key'].nil? }
  only_if { node['gitlab_fluentd']['pubsub_enable'] }
end

file node['gitlab_fluentd']['pubsub_file'] do
  action :delete
  notifies :restart, 'service[td-agent]', :delayed
  not_if { node['gitlab_fluentd']['pubsub_enable'] }
end

cookbook_file '/etc/td-agent/plugin/out_cloud_pubsub.rb' do
  owner 'root'
  group 'root'
  mode '0644'
  notifies :restart, 'service[td-agent]', :delayed
  only_if { node['gitlab_fluentd']['pubsub_enable'] }
end

cookbook_file '/etc/td-agent/prometheus-mixin.conf' do
  owner 'root'
  group 'root'
  mode '0644'
  notifies :restart, 'service[td-agent]', :delayed
end

file '/etc/td-agent/plugin/out_cloud_pubsub.rb' do
  action :delete
  notifies :restart, 'service[td-agent]', :delayed
  not_if { node['gitlab_fluentd']['pubsub_enable'] }
end

# old plugin name, deprecated but we ensure it is
# deleted if it still exists.
file '/etc/td-agent/plugin/plugin-cloud-pubsub.rb' do
  action :delete
  notifies :restart, 'service[td-agent]', :delayed
end

## Install Gems

# The pg gem, used by fluent-plugin-postgresql-csvlog requires `pg_config`
# which is part of libpq-dev
# So install this here instead of in the posgres recipe to make sure this dependency
# is installed before we try to install the gems.
apt_package 'libpq-dev' do
  action :install
  only_if { node['gitlab_fluentd']['enable_postgres_csvlog_ingestion'] }
end

cookbook_file node['gitlab_fluentd']['gemfile'] do
  owner 'root'
  group 'root'
  mode '0644'
end

cookbook_file "#{node['gitlab_fluentd']['gemfile']}.lock" do
  owner 'root'
  group 'root'
  mode '0644'
  notifies :run, 'execute[install td-agent gems]', :immediately
end

# td-agent will run bundle install at startup, but this may take a while
# and may time out systemd.
execute 'install td-agent gems' do
  bundle_groups_for_recipes = node['recipes'].map { |r| r.split('::').last }.join(':')
  bundle_install = "/opt/td-agent/bin/bundle install --deployment --gemfile=#{node['gitlab_fluentd']['gemfile']} --path=#{node['gitlab_fluentd']['gem_path']}"
  bundle_install = [bundle_install, "--with=#{bundle_groups_for_recipes}"].join(' ')

  command bundle_install
  user 'td-agent'
  group 'td-agent'
  action :nothing
  notifies :restart, 'service[td-agent]', :delayed
end

# td-agent is started in its package's postinst.
service 'td-agent' do
  action :enable
end

logrotate_app 'td-agent' do
  path '/var/log/td-agent/td-agent.log'
  options %w(missingok compress delaycompress notifempty)
  rotate 7
  frequency 'daily'
  size '1G'
  postrotate <<-EOF
pid=/var/run/td-agent/td-agent.pid
    if [ -s "$pid" ]
    then
      kill -USR1 "$(cat $pid)"
    fi
  EOF
end
