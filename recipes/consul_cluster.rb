# Cookbook:: gitlab_fluentd
# Recipe:: consul_cluster
# License:: MIT
#
# Copyright:: 2018, GitLab Inc.

include_recipe 'gitlab_fluentd::default'

template File.join(node['gitlab_fluentd']['config_dir_modules'], 'consul_cluster.conf') do
  owner 'root'
  group 'root'
  mode '0644'
  notifies :restart, 'service[td-agent]', :delayed
end
