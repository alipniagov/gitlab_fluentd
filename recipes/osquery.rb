# Cookbook:: gitlab_fluentd
# Recipe:: osquery
# License:: MIT
#
# Copyright:: 2021, GitLab Inc.

include_recipe 'gitlab_fluentd::default'

template File.join(node['gitlab_fluentd']['config_dir_modules'], 'osquery.conf') do
  owner 'root'
  group 'root'
  mode '0644'
  notifies :restart, 'service[td-agent]', :delayed
end
