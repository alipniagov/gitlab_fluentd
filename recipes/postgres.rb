# Cookbook:: gitlab_fluentd
# Recipe:: postgres
# License:: MIT
#
# Copyright:: 2018, GitLab Inc.

include_recipe 'gitlab_fluentd::default'

# Enable these by default on nodes ingesting postgres information
node.default['gitlab_fluentd']['enable_postgres_csvlog_ingestion'] = true

# These files have been moved to to a plugin:
# https://gitlab.com/gitlab-org/fluent-plugins/fluent-plugin-postgresql-csvlog
file '/etc/td-agent/plugin/parser_multiline_csv.rb' do
  action :delete
  notifies :restart, 'service[td-agent]', :delayed
end

file '/etc/td-agent/plugin/filter_postgresql_slowlog.rb' do
  action :delete
  notifies :restart, 'service[td-agent]', :delayed
end

template File.join(node['gitlab_fluentd']['config_dir_modules'], 'postgres.conf') do
  owner 'root'
  group 'root'
  mode '0644'
  notifies :restart, 'service[td-agent]', :delayed
end
