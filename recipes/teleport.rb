# Cookbook:: gitlab_fluentd
# Recipe:: teleport
# License:: MIT
#
# Copyright:: 2022, GitLab Inc.

include_recipe 'gitlab_fluentd::default'

template File.join(node['gitlab_fluentd']['config_dir_modules'], 'teleport_events.conf') do
  owner 'root'
  group 'root'
  mode '0644'
  notifies :restart, 'service[td-agent]', :delayed
end
